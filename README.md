# devops-netologyFirst_String

File that will be ignored during commit because they are  pointed  out in  file  .gitignore:

1. Files that located in  "terraform"  directory.
2. Log files (files with ".log"  extentions)
3. Files that contains sensitive data  ( .tfvars , .tfvars.json )
4. Files  that override  config   (override.tf)
5. Configuration  files ( terraform.rc ) 
